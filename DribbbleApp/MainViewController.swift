//
//  ViewController.swift
//  DribbbleApp
//
//  Created by Nikita Feshchun on 23.07.16.
//  Copyright © 2016 Igor Feshchun. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class MainViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //MARK: Properties
    
    @IBOutlet var shotCollectionView: UICollectionView!
    @IBOutlet var shotCollectionLayout: UICollectionViewFlowLayout!
    
    //MARK: Variables
    let reuseIdentifier = "shot_cell"
    let segueIdentifier = "to_details"
    let defaultPage = 1
    var currentPage = 1
    let realm = try! Realm()
    lazy var shots: Results<Shot> = { self.realm.objects(Shot.self) }()
    var refreshControl: UIRefreshControl!
    var loadingData = false
    let imageChache = SDImageCache.sharedImageCache()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentPage = 1
        let cellSize = calcCellSize(self.view.frame.size)
        shotCollectionLayout.itemSize = CGSizeMake(cellSize.cellWidth, cellSize.cellHeight)
        
        if ConnectionChecker.isConnectedToNetwork(){
            loadShots(true, pageToLoad: defaultPage)
        } else {
            collectionView?.reloadData()
            infoAlert("Network", message: "Unavailable")
        }
        
        setupResreshControl()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setuping refresh control
    func setupResreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(MainViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        collectionView?.addSubview(refreshControl)
        
    }
    
    //MARK: Handle pull refreshing
    func refresh(sender:AnyObject) {
        if ConnectionChecker.isConnectedToNetwork(){
            currentPage = 1
            sender.beginRefreshing()
            loadShots(true, pageToLoad: defaultPage)
            collectionView?.reloadData()
            imageChache.clearDisk()
            imageChache.clearMemory()
            sender.endRefreshing()
        } else{
            infoAlert("Network", message: "Unavailable")
            sender.endRefreshing()
        }
    }
    
    //MARK: Download shots from Dribbble
    func loadShots(newRequest: Bool, pageToLoad: Int){
        if newRequest{
            imageChache.clearMemory()
            imageChache.clearDisk()
        }
        DribbbleApi.get(Shot.self, collection: collectionView!, page: pageToLoad, newRequest: newRequest)
    }
    
    //MARK: Initialization of collection
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shots.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ShotCell
        let shot = shots[indexPath.row]
        
        // Here description cleans from html tags
        if (shot.shotDescription == ""){
            cell.shotDescription.text = "No description"
        } else {
            cell.shotDescription.text = shot.shotDescription.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
        }
        
        cell.shotTitle.text = shot.title.capitalizedString
        
        loadImage(cell.shotImageView, indexPath: indexPath)
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if ConnectionChecker.isConnectedToNetwork(){
            if indexPath.row == (shots.count - 1){
                currentPage += 1
                loadShots(false, pageToLoad: currentPage)
                print(currentPage)
            }
        }
    }
    
    //MARK: Segue preparation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == segueIdentifier){
            let selectedItems = collectionView!.indexPathsForSelectedItems()

            if let sItem = selectedItems as [NSIndexPath]!{
                let shot = shots[sItem[0].row]
                let controller = segue.destinationViewController as! ShotDetailViewController
                controller.shot = shot
                controller.imageCache = imageChache
                
            }
            
        }
        
    }
    //MARK: Handle cell size displaying
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        let cellSize = calcCellSize(size)
        shotCollectionLayout.itemSize = CGSizeMake(cellSize.cellWidth, cellSize.cellHeight)
    }
    
    //MARK: calculate cell size
    func calcCellSize(size: CGSize) -> (cellHeight: CGFloat, cellWidth: CGFloat) {
        let transitionToWide = size.width > size.height
        var cellHeight = size.height/2
        var cellWidth = size.width
        
        if transitionToWide {
            cellWidth = size.width / 2
            cellHeight = size.height / 2 + size.height / 3
        }
        return (cellHeight, cellWidth)
    }
    
    //MARK: show alert if something wrond
    func infoAlert(title: String, message: String) -> Void {
        let actionSheetController: UIAlertController = UIAlertController(title:title, message:message, preferredStyle: .Alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in }
        actionSheetController.addAction(cancelAction)
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: load images from url
    func loadImage(imageView: UIImageView, indexPath: NSIndexPath) {
        var URL: NSURL?
        let shot = shots[indexPath.row]
        if shot.imagesHIDPI != "" {
            URL = NSURL(string: shot.imagesHIDPI)!
        } else {
            URL = NSURL(string: shot.imagesNormal)!
        }
        guard URL != nil else {
            return
        }
        imageView.sd_setImageWithURL(URL, placeholderImage: UIImage(named: "Image"))
        self.loadingData = false
        
    }
    
}

