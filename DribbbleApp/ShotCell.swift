//
//  ShotCell.swift
//  DribbbleApp
//
//  Created by Nikita Feshchun on 23.07.16.
//  Copyright © 2016 Igor Feshchun. All rights reserved.
//

import UIKit

class ShotCell: UICollectionViewCell {
    
    //MARK: Properties
    @IBOutlet weak var shotDescription: UILabel!
    @IBOutlet weak var shotTitle: UILabel!
    @IBOutlet weak var shotImageView: UIImageView!
    @IBOutlet weak var shotCellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shotImageView.contentMode = .ScaleAspectFill
       
        
    }
    
}
