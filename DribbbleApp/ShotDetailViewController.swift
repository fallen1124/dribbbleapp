//
//  ShotDetailViewController.swift
//  DribbbleApp
//
//  Created by Nikita Feshchun on 23.07.16.
//  Copyright © 2016 Igor Feshchun. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ShotDetailViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var shotDescription: UILabel!
    @IBOutlet weak var shotTitle: UILabel!
    @IBOutlet weak var shotImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: Variables
    var URL: NSURL!
    var imageCache = SDImageCache.sharedImageCache()
    var shot : Shot!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "\(shot.title.capitalizedString)"
        
        if (shot.shotDescription == ""){
            shotDescription.text = "No description"
        } else {
            shotDescription.text = shot.shotDescription.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
        }
        
        shotTitle.text = shot.title.capitalizedString
        
        if shot.imagesHIDPI != ""{
            URL = NSURL(string: shot.imagesHIDPI)
        } else {
            URL = NSURL(string: shot.imagesNormal)
        }
        
        shotImage.sd_setImageWithURL(URL!)
    }
    
}
