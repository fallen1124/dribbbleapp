//
//  Shot.swift
//  DribbbleApp
//
//  Created by Nikita Feshchun on 24.07.16.
//  Copyright © 2016 Igor Feshchun. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Shot: Object, Mappable{
    dynamic var id = 0
    dynamic var title = ""
    dynamic var date = ""
    dynamic var animated = 0
    dynamic var shotDescription = ""
    dynamic var likesCount =  0
    dynamic var viewsCount = 0
    dynamic var imagesHIDPI = ""
    dynamic var imagesNormal = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    //Impl. of Mappable protocol
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        id <- map["id"]
        date <- map["date"]
        animated <- map["animated"]
        shotDescription <- map["description"]
        likesCount <- map["likes_count"]
        viewsCount <- map["views_count"]
        imagesHIDPI <- map["images.hidpi"]
        imagesNormal <- map["images.normal"]
        
    }

}